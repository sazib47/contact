var app = angular.module("contact", []);

app.controller("ContactCtrl", ["$scope", "$http", "$q", '$timeout',
    function ($scope, $http, $q, $timeout) {
        $scope.form = {};
        $scope.form2 = {};
        var url = "http://127.0.0.1:8000/api/contact/";

         // generate success message
        success_message = function(msg){
            $scope.success = true;
            $scope.success_msg = msg;
            $timeout(function(){
                $scope.success = false;
            }, 2000);
        }

        // generate error message
        error_message = function(msg){
            $scope.failed = true;
            $scope.error_msg = msg;
            $timeout(function(){
                $scope.failed = false;
            }, 2000);
        }

        var get = function () {
            function onSuccess(response) {
                $scope.contacts = response.data;
            }

            function onError(errors) {
                console.log(errors);
            }

            $http.get(url).then(onSuccess).catch(onError);
        };

        // Delete Specific contact
        $scope.deleteContact = function (id) {
            function onSuccess() {
                get();
            }

            function onError(errors) {
                console.log(errors);
            }
          
            $http.delete(url + id + "/").then(onSuccess).catch(onError);
        };

        
        // Set vaule in update form fields
        $scope.valueInUpdateForm = function(name, phone){
           $scope.form2={
                'name':name,
                'phone':phone
           }
        };

        // Update contact
        $scope.updateContact = function(id){
            var data = $scope.form2;

            function onSuccess(response, status, hearder, config){
                // loading latest contact list
                get();
                // generate success message
                success_message("Contact Updated Successfully");
            }

            function onError(error, status, hearder, config){
                error_message("Something is going wrong");
            }

            $http.put(url + id + "/", data).then(onSuccess).catch(onError);
        };

        // add contact 
       $scope.addContact = function(){
            var data = $scope.form;
            
            function onSuccess(response, status, hearder, config){
                $scope.form ={};
                // loading latest contact list
                get();
                // generate success message
                success_message("New contact Added Successfuly");
            }

            function onError(error, status, hearder, config){
                error_message("somethings went wrong, fill up the fields correctly");
            }

            $http.post(url, data).then(onSuccess).catch(onError);
            
        };

        // loading the list 
        get();
    }
]);